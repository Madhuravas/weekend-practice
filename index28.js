const data = [
    {
        place: "Melbourne",
        country: "Australia",
        location: {
            lat: '99',
            lng: '88'
        },
        temperature: '38 Degree Celsius'
    }, {
        place: "New Delhi",
        country: "India",
        location: {
            lat: '84',
            lng: '44'
        },
        temperature: '42 Degree Celsius'
    }, {
        place: "Pretoria",
        country: "SouthAfrica",
        location: {
            lat: '35',
            lng: '24'
        },
        temperature: '39 Degree Celsius'
    }, {
        place: "Mexico City",
        country: "Mexico",
        location: {
            lat: '34',
            lng: '38'
        },
        temperature: '43 Degree Celsius'
    },
    {
        place: "London",
        country: "England",
        location: {
            lat: '57',
            lng: '34'
        },
        temperature: '26 Degree Celsius'
    }
]


/*
Note: Make sure not to mutate the array in all of the following questions.


Q1. Get all latitude and longitude of all the places in the following manner.
[{place: (lat, long)}, ...]

Q2. Sort data based on temperature (Low temperature first).

Q3.Rearrange data in the following format
[{ country: { place: { location: {lat, lng }, temperature }}}, ...]

Q4. Change temperature of SouthAfrica "Pretoria" to "49 Degree Celsius".

Q5. Add a new Object in the fourth postiion.
{ 
        place: "Bangalore", 
        country: "India", 
        location: {
            lat: '84',
            lng: '47'
        },
        temperature: '29 Degree Celsius'
 }

 Q6. Delete the third element in the array .
 Q7. Swap elements at position 2 and second last.
*/

// Question 1 

const latitudeAndLongitude = data.map((eachPlace) => {
    let obj = {}
    obj[eachPlace.place] = eachPlace.location
    return obj
})

//console.log(latitudeAndLongitude)

// Question 2 

const sortDataOnTemp = (data) => {
    let newData = [...data]
    newData.sort((a, b) => {
        let temperatureA = a.temperature.split(" ")[0]
        let temperatureB = b.temperature.split(" ")[0]

        return temperatureA > temperatureB ? 1 : -1
    })
    console.log(newData)

}

//sortDataOnTemp(data)

// Question 3 

const newArr = data.map((eachObj) => {
    const obj = {}
    const data = {}
    const { country, place, ...rest } = eachObj
    data[place] = rest
    obj[country] = data
    return obj
})
//console.log(newArr)
fs.writeFileSync("./data3.json",JSON.stringify(newArr))

// Question 4 

let changeSATemperature = data.map((eachPlace) => {
    if (eachPlace.country === "SouthAfrica") {
        let detailsOfSA = { ...eachPlace }
        detailsOfSA.temperature = "49 Degree Celsius"
        return detailsOfSA
    } else {
        return eachPlace
    }
})
console.log(changeSATemperature)
console.log(data)



// Question 5 

const extraData = {
    place: "Bangalore",
    country: "India",
    location: {
        lat: '84',
        lng: '47'
    },
    temperature: '29 Degree Celsius'
}

const newDataAddedToArr = [...data.slice(0, 3), extraData, ...data.slice(3)]

//console.log(newDataAddedToArr)

// Question 6


let copyArr = [...data]

copyArr.splice(2, 1)
//console.log(copyArr)


//Question 7 

const swapArr = [...data]

const secondPositionObj = swapArr[1]
const lastSecondPositionObj = swapArr[data.length - 2]

swapArr.splice(1, 1, lastSecondPositionObj)
swapArr.splice(data.length - 2, 1, secondPositionObj)
//console.log(swapArr)


